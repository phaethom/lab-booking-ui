
import 'antd/dist/antd.css';
import { Layout, Menu, Breadcrumb, Select } from 'antd';
import { UserOutlined} from '@ant-design/icons';
import Calendar from '../../calendar/Calendar';


import { Container, LogoContainer, LogoImage, LabTitle, LabTitleContainer } from './style';

import logo from '../../../img/logo.png';

const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout

const { Option } = Select;
function handleChange(value) {
  console.log(value);
}

function Home() {

  return (
    <Layout>
      <Header className="header">
        <LogoContainer>
          <LogoImage src={logo} alt="Titania Logo" />
        </LogoContainer>
      </Header>
      <Content style={{ padding: '0 50px' }}>

        <Breadcrumb style={{ margin: '16px 0' }}>
          <LabTitleContainer>
            <LabTitle>Lab Booking System</LabTitle>
          </LabTitleContainer>
        </Breadcrumb>

        <Layout className="site-layout-background" style={{ padding: '24px 0' }}>
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              style={{ height: '100%' }}
            >
              <SubMenu key="sub1" icon={<UserOutlined />} title="John">
                <Menu.Item key="1">My Bookings</Menu.Item>
                <Menu.Item key="2">Test Logs</Menu.Item>
                <Menu.Item key="3">Logout</Menu.Item>
              </SubMenu>

            </Menu>
          </Sider>
          <Content style={{ padding: '0 24px', minHeight: 280 }}>
            <Calendar />
          </Content>
        </Layout>
      </Content>
      <Footer style={{ textAlign: 'center' }}> ©Titania 2021. </Footer>
    </Layout>
  )
}

export default Home;
