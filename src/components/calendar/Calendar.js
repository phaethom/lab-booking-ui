import React, {useState, useEffect } from 'react';
import { Inject, ScheduleComponent, Day, Week, WorkWeek, Month, Agenda } from '@syncfusion/ej2-react-schedule';
import {Container} from './style';
import * as dataSource from '../../__mock/datasource.json';
import Lab from '../../api/Lab';

const Calendar = () =>{

    const [results, setResults] = useState([]);
    console.log(dataSource.scheduleData)
    const fetchEvents = async () =>{
    const response = await Lab.get('/bookings');
       //console.log(response);
    setResults(response.data.content);
    }

    useEffect(() => {
        fetchEvents();
    },[])

    return(
        <Container>
            {console.log(results)}
            <ScheduleComponent currentView='Month' eventSettings= {{ dataSource: dataSource.scheduleData }} >
                <Inject services={[Day, Week, WorkWeek, Month, Agenda]} />
            </ScheduleComponent>
        </Container>
    )
}

export default Calendar;